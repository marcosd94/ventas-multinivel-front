app.controller('ProductosListCtrl', ['$scope', 'ProductosService', '$controller',
    function ($scope, service, $controller) {



        /**
         * Service utilizdo para recuperar los datos y realizar las operaciones.
         * @field
         * @type {Object}
         */
        $scope.service = service;

        $scope.uri = "/productos/";
        $scope.nombre= 'Productos';

        /**
         * Configuraciones de la cabecera de la grilla.
         * @private
         * @type {Array}
         */
        var header = [
            {
                "key": "descripcion",
                "name": "Producto"
            },
            {
                "key": "precioVenta",
                "name": "Precio Venta"
            },
            {
                "key": "idTipoProducto",
                "name": "Tipo Producto"
            },
            {
                "key": "tipoIva",
                "name": "Tipo Iva"
            },
            {
                "key": "acciones",
                "name": "Acciones"
            }
        ];

        /**
         * Constructor / Entrypoint
         * @constructor
         */
        (function initialize() {
            // se hereda del controller base
            angular.extend(this, $controller('BaseListCtrl', {
                "$scope": $scope
            }));
            $scope.config.header = header;
            $scope.config.recurso = 'productos';
        })();
}]);
