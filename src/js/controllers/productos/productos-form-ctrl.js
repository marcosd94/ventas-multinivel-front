app.controller('ProductosFormCtrl', ['$scope', 'ProductosService', '$controller',
    function ($scope, service, $controller) {

        /**
         * Service utilizdo para recuperar los datos y realizar las operaciones.
         * @field
         * @type {Object}
         */
        $scope.service = service;

        /**
         * Url del recurso
         * @field
         * @type {Object}
         */
        $scope.uri = "/productos/";
        $scope.nombre = "Productos";


        $scope.listaTipoProducto = [{
            id: 1,
            descripcion: 'EQU'
        },{
            id: 2,
            descripcion: 'ACC'
        }];

        $scope.listaIva = [{
            id: 1,
            descripcion: 'EQU'
        },{
            id: 2,
            descripcion: 'ACC'
        }];
        /**
         * Constructor / Entrypoint
         * @constructor
         */
        (function initialize() {

            $scope.listaTipoProducto = [{
                id: 1,
                descripcion: 'EQU'
            },{
                id: 2,
                descripcion: 'ACC'
            }];
            // se hereda del controller base
            angular.extend(this, $controller('BaseFormCtrl', {
                "$scope": $scope
            }));
        })();
    }
]);
