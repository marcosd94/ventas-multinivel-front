app.controller('ProveedoresListCtrl', ['$scope', 'ProveedoresService', '$controller',
    function ($scope, service, $controller) {



        /**
         * Service utilizdo para recuperar los datos y realizar las operaciones.
         * @field
         * @type {Object}
         */
        $scope.service = service;

        $scope.uri = "/proveedores/";
        $scope.nombre= 'Proveedores';

        /**
         * Configuraciones de la cabecera de la grilla.
         * @private
         * @type {Array}
         */
        var header = [
            {
                "key": "nombre",
                "name": "Nombre del Proveedor"
            },
            {
                "key": "ruc",
                "name": "Ruc"
            },
            {
                "key": "telefono",
                "name": "Teléfono"
            },
            {
                "key": "direccion",
                "name": "Dirección"
            },
            {
                "key": "acciones",
                "name": "Acciones"
            }
        ];

        /**
         * Constructor / Entrypoint
         * @constructor
         */
        (function initialize() {
            // se hereda del controller base
            angular.extend(this, $controller('BaseListCtrl', {
                "$scope": $scope
            }));
            $scope.config.header = header;
            $scope.config.recurso = 'proveedores';
        })();
}]);
