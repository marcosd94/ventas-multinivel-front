app.service('ProductosService', ['$http', 'BaseService', function ($http, BaseService) {

    return angular.extend({}, BaseService, {
        recurso: "productos",
        nombre: "Productos",
        /**
         * Se sobrescribe el método base para invocar a la url del recurso paginado.
         * Esto es debido a que se utiliza un json server para simular una api rest.
         * @function
         */
        listar: function (params) {
            return $http.get(App.REST_BASE + "productos/listar", {
                params: params
            });
        }
    });
}]);
