
app.service('MediosPagosService', ['$http', 'BaseService', function ($http, BaseService) {

    return angular.extend({}, BaseService, {
        recurso: "medios-pagos",
        nombre: "Medios de Pago",
        /**
         * Se sobrescribe el método base para invocar a la url del recurso paginado.
         * @function
         */
        listar: function (params) {
            return $http.get(App.REST_BASE + "medios-pagos/listar", {
                params: params
            });
        }
    });
}]);
