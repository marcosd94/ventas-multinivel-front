/*
 * Se configuran las rutas de la aplicación
 */
app.config(['$routeProvider', '$httpProvider', 'keycloakLauncherProvider',
    function ($routeProvider, $httpProvider, keycloakLauncherProvider) {
        /*
         * Se registran los interceptors de keycloak.
         */
        $httpProvider.interceptors.push('errorInterceptor');
        $httpProvider.interceptors.push('authInterceptor');
        /*
         * Route resolver que se utiliza para restringir las páginas que necesitan 
         * que el usuario este logeado en el sistema para acceder a al misma. 
         */
        var resolve = {
            init: ['keycloakLauncher', '$location', function (keycloakLauncher, $location) {
                if (!keycloakLauncher.loggedIn) {
                    $location.url("/");
                }
            }]
        };

        /*
         * Route resolver que se utiliza para evitar que se accedan a páginas de caracter público
         * cuando el usuario está logeado.
         */
        var publicResolve = {
            init: ['keycloakLauncher', '$location', function (keycloakLauncher, $location) {
                if (keycloakLauncher.loggedIn) {
                    $location.url("/dashboard");
                }
            }]
        };

        /*
         * Se definen las rutas de la aplicación
         */
        $routeProvider
            .when('/', {
                templateUrl: 'partials/public-partial.html',
                resolve: publicResolve
            })
            .when('/dashboard', {
                templateUrl: 'partials/dashboard-partial.html',
                resolve: resolve
            })
            /*.when('/medios-pagos/', {
                templateUrl: 'partials/empresas/empresa-list-partial.html',
                controller: 'EmpresaListCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/crear', {
                templateUrl: 'partials/empresas/medios-pagos-form-partial.html',
                controller: 'EmpresaFormCtrl',
                resolve: resolve
            })
            .when('/empresas/:id/editar', {
                templateUrl: 'partials/empresas/medios-pagos-form-partial.html',
                controller: 'EmpresaFormCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/:id/ver', {
                templateUrl: 'partials/empresas/empresa-view-partial.html',
                controller: 'EmpresaViewCtrl',
                resolve: resolve
            })*/
            /*.when('/medios-pagos/', {
                templateUrl: 'partials/empresas-template/empresa-list-partial.html',
                controller: 'EmpresaTemplateListCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/crear', {
                templateUrl: 'partials/empresas-template/medios-pagos-form-partial.html',
                controller: 'EmpresaTemplateFormCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/:id/editar', {
                templateUrl: 'partials/empresas-template/medios-pagos-form-partial.html',
                controller: 'EmpresaTemplateFormCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/:id/ver', {
                templateUrl: 'partials/empresas-template/empresa-view-partial.html',
                controller: 'EmpresaTemplateViewCtrl',
                resolve: resolve
            })*/

            /**
             * MEDIOS DE PAGOS
             **/

            .when('/medios-pagos/', {
                templateUrl: 'partials/medios-pagos/medios-pagos-list-partial.html',
                controller: 'MediosPagosListCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/crear', {
                templateUrl: 'partials/medios-pagos/medios-pagos-form-partial.html',
                controller: 'MediosPagosFormCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/:id/editar', {
                templateUrl: 'partials/medios-pagos/medios-pagos-form-partial.html',
                controller: 'MediosPagosFormCtrl',
                resolve: resolve
            })
            .when('/medios-pagos/:id/ver', {
                templateUrl: 'partials/medios-pagos/medios-pagos-view-partial.html',
                controller: 'MediosPagosViewCtrl',
                resolve: resolve
            })


            /**
             * PROVEEDORES
             **/

            .when('/proveedores/', {
                templateUrl: 'partials/proveedores/proveedores-list-partial.html',
                controller: 'ProveedoresListCtrl',
                resolve: resolve
            })
            .when('/proveedores/crear', {
                templateUrl: 'partials/proveedores/proveedores-form-partial.html',
                controller: 'ProveedoresFormCtrl',
                resolve: resolve
            })
            .when('/proveedores/:id/editar', {
                templateUrl: 'partials/proveedores/proveedores-form-partial.html',
                controller: 'ProveedoresFormCtrl',
                resolve: resolve
            })
            .when('/proveedores/:id/ver', {
                templateUrl: 'partials/proveedores/proveedores-view-partial.html',
                controller: 'ProveedoresViewCtrl',
                resolve: resolve
            })


            /**
             * PRODUCTOS
             **/

            .when('/productos/', {
                templateUrl: 'partials/productos/productos-list-partial.html',
                controller: 'ProductosListCtrl',
                resolve: resolve
            })
            .when('/productos/crear', {
                templateUrl: 'partials/productos/productos-form-partial.html',
                controller: 'ProductosFormCtrl',
                resolve: resolve
            })
            .when('/productos/:id/editar', {
                templateUrl: 'partials/productos/productos-form-partial.html',
                controller: 'ProductosFormCtrl',
                resolve: resolve
            })
            .when('/productos/:id/ver', {
                templateUrl: 'partials/productos/productos-view-partial.html',
                controller: 'ProductosViewCtrl',
                resolve: resolve
            })
            //finaly
            .otherwise({
                redirectTo: '/dashboard'
            });
}]);


/**
 * Se configura para que google analytis que trackee las páginas visitadas.
 */
app.run(['$rootScope', '$location', '$window', function ($rootScope, $location, $window) {
    // initialise google analytics
    //$window.ga('create', 'UA-XXXXXXXX-X', 'auto');
    // track pageview on state change
    $rootScope.$on('$routeChangeStart', function (event) {
        //$window.ga('send', 'pageview', $location.path());
    });
}]);
